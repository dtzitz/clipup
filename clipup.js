Kids = new Mongo.Collection("kids");
Scores = new Mongo.Collection("scores");
Rewards = new Mongo.Collection("rewards");

//call to query client side mongo


if (Meteor.isClient) {
  Meteor.subscribe("kids")
  Meteor.subscribe("scores")
  Meteor.subscribe("rewards")

  Meteor.startup(function() {
    Session.setDefault("templateName", "overview")
    console.log("front end firing")
  });


  Template.body.helpers({
    template_name: function() {
      return Session.get("templateName")
    },

    kids: function() {
      return Kids.find({});
    },
    current_kid: function() {
      return Session.get("currentKid");
    }

  });

  //**********************LISTENER FOR NAV ELEMENTS **************************//
  Template.body.events({
    "click .overview": function() {
      Session.set("templateName", "overview")
    },
    "click .newkid": function() {
      Session.set("templateName", "newkid")
    },
    "click .record": function() {
      Session.set("templateName", "record")
      Session.set("currentKid", this._id)
    },
    "click .rewards": function() {
      Session.set("templateName", "rewards")
      Session.set("currentKid", this._id)
    },
    "click .reports": function() {
      Session.set("templateName", "reports")
    },
    "click .help": function() {
      Session.set("templateName", "help")
    }
  });

//*************************OVERVIEW CONTROLLER*********************************//
  Template.overview.helpers({
    kids: function() {
      return Kids.find({});
    },
    scores:function() {
      return Scores.find({kid : this._id},{sort: {point_date:-1} });
    },
    available_rewards:function(){
      rewards = Rewards.find({ "kid" : this._id });
      today = moment().format("MM/DD/YYYY")
      todays_score = Scores.findOne({
        kid: this._id,
        point_date: today
      })
      
      
      available_rewards_array = []

      rewards.forEach(function (rewards) {
        if (todays_score.point_value >= rewards.required_points){
          available_rewards_array.push(rewards.reward_name)
        }

      });

      
      return available_rewards_array;
      
    },

//return image url for correct avatar
    

//calculate weekly average
    averagescore: function(){
      scores = Scores.find({kid : this._id},{sort: {point_date:-1} })
      console.log("Hello Jasmine")
      //iterate over the score/date hash to implement date logic
      i = 0
      points_total = 0
       
      scores.forEach(function (scores) {
        // for each scores as defined above iterate through and calculate weekly average
        console.log("why does this work but not the other thing?")
        eval_date = moment(scores.point_date, "MM DD YYYY")
        end_of_week = moment().isoWeekday(5)
        start_of_week = moment().isoWeekday(0)
        
        if (eval_date < end_of_week && eval_date >= start_of_week){
          i++
          points_total += parseInt(scores.point_value)
        }
        
      })
      average_score = points_total/i
      
      return average_score;

      },

//return the URL for apropriate avatar
      avatar_url:function(){
        //going to repeat the code for average score; this is bad and I should feel bad
        scores = Scores.find({kid : this._id},{sort: {point_date:-1} })
        kid = Kids.findOne({_id: this._id});
        
        i = 0
        points_total = 0
         
        scores.forEach(function (scores) {
          // for each scores as defined above iterate through and calculate weekly average
          
          eval_date = moment(scores.point_date, "MM DD YYYY")
          end_of_week = moment().isoWeekday(5)
          start_of_week = moment().isoWeekday(0)
          
          if (eval_date < end_of_week && eval_date >= start_of_week){
            i++
            points_total += parseInt(scores.point_value)
          }
          
        })
        
        
        //"avatar_red" "avatar_purple" "avatar_blue" "avatar_yellow" "avatar_green" "avatar_black"
        if (kid.avatar == "avatar_red"){
          if(points_total < 4 )
            return Spacebars.SafeString('http://i.imgur.com/2D3QDXC.jpg')

          if(points_total < 8)
            return Spacebars.SafeString('http://i.imgur.com/CSu7hr3.jpg')

          if(points_total < 12)
            return Spacebars.SafeString('http://i.imgur.com/Avc1fmV.jpg')

          if(points_total < 16)
            return Spacebars.SafeString('http://i.imgur.com/R9n5lIh.jpg')
          else
            return Spacebars.SafeString('http://i.imgur.com/ZEqv6sg.jpg')
          }

        if (kid.avatar == "avatar_purple"){
          if(points_total < 4 )
            return Spacebars.SafeString('http://i.imgur.com/2SRzg5e.jpg')

          if(points_total < 8)
            return Spacebars.SafeString('http://i.imgur.com/vVi7sSV.jpg')

          if(points_total < 12)
            return Spacebars.SafeString('http://i.imgur.com/QRXyf1N.jpg')

          if(points_total < 16)
            return Spacebars.SafeString('http://i.imgur.com/MHpQmDV.jpg')
          else
            return Spacebars.SafeString('http://i.imgur.com/pdnGULd.jpg')
        }

        if (kid.avatar == "avatar_blue"){
          if(points_total < 4 )
            return Spacebars.SafeString('http://i.imgur.com/Zx7Dfwd.jpg')

          if(points_total < 8)
            return Spacebars.SafeString('http://i.imgur.com/W96alog.jpg')

          if(points_total < 12)
            return Spacebars.SafeString('http://i.imgur.com/i0oTdYE.jpg')

          if(points_total < 16)
            return Spacebars.SafeString('http://i.imgur.com/lw2wUQf.jpg')
          else
            return Spacebars.SafeString('http://i.imgur.com/KRwvBYe.jpg')
        }

        if (kid.avatar == "avatar_black"){
          if(points_total < 4 )
            return Spacebars.SafeString('http://i.imgur.com/bzuhnJ0.jpg')

          if(points_total < 8)
            return Spacebars.SafeString('http://i.imgur.com/aiJF3hI.jpg')

          if(points_total < 12)
            return Spacebars.SafeString('http://i.imgur.com/jZn4jhC.jpg')

          if(points_total < 16)
            return Spacebars.SafeString('http://i.imgur.com/sSNIttz.jpg')
          else
            return Spacebars.SafeString('http://i.imgur.com/LWe6Mkt.jpg')
        }

        if (kid.avatar == "avatar_yellow"){
          if(points_total < 4 )
            return Spacebars.SafeString('http://i.imgur.com/7LI4BM2.jpg')

          if(points_total < 8)
            return Spacebars.SafeString('http://i.imgur.com/YGLOqI7.jpg')

          if(points_total < 12)
            return Spacebars.SafeString('http://i.imgur.com/gEtGhOI.jpg')

          if(points_total < 16)
            return Spacebars.SafeString('http://i.imgur.com/t2cKLWU.jpg')
          else
            return Spacebars.SafeString('http://i.imgur.com/8ArRAmH.jpg')
        }

        if (kid.avatar == "avatar_green"){
          if(points_total < 4 )
            return Spacebars.SafeString('http://i.imgur.com/h0PvmSt.jpg')

          if(points_total < 8)
            return Spacebars.SafeString('http://i.imgur.com/cN9VjIi.jpg')

          if(points_total < 12)
            return Spacebars.SafeString('http://i.imgur.com/V3HqPSQ.jpg')

          if(points_total < 16)
            return Spacebars.SafeString('http://i.imgur.com/5L7JOTr.jpg')
          else
            return Spacebars.SafeString('http://i.imgur.com/jtkVSzU.jpg')
        }
      }
  });
  

  //*************************Record Points************************//

  Template.record.rendered = function() {
    $('#datetimepicker').datetimepicker({
      format: 'L'
    })
    var range = $('.input-range')
    value = $('.range-value')
    value.html(range.attr('value'))

    range.on('input', function() {
      value.html(this.value);
    })

  }
  Template.record.helpers({

    kids: function() {
      return Kids.find(Session.get("currentKid"))
    }

  })

  Template.record.events({
    "submit .update_points": function(event) {
      event.preventDefault()
      var point_date = event.target.date.value
      var point_value = event.target.points.value
      kid = this._id

      Meteor.call("update_points", kid, point_date, point_value)
      Session.set("templateName", "overview");

    }
  })
//***********************Set rewards******************************//
  Template.rewards.helpers({
    kids: function() {
      return Kids.find(Session.get("currentKid"))
    },

    scores: function(){
      return Scores.find({kid : this._id},{sort: {point_date:-1} });
    }

  })

  Template.rewards.events({
    'submit .update_rewards': function (event) {
      var reward_name = event.target.reward_name.value
      var required_points = event.target.required_points.value
      kid = this._id
      console.log("posting/updating rewards")
      Meteor.call('update_rewards', kid, reward_name, required_points );
    }

  })


  //***************************ADD/REMOVE KIDS *********************//
  Template.newkid.helpers({
    kids: function() {
      return Kids.find({});
    }
  });


  Template.newkid.events({
    "submit .new-kid": function(event) {
      var new_avatar = $('input[type="radio"][name="avatar_select"]:checked').val(); //jquery
      var kidname = event.target.name.value
      var kidgrade = event.target.grade.value
      var kid_born = new Date()
      console.log("adding new kid")
      Meteor.call("add_kid", kidname, kidgrade, new_avatar, kid_born)

    },
    "submit .delete_kid": function(event) {

      if (window.confirm("This will remove the kid and all thier records. This can not be undone")) {
        Meteor.call("delete_kid", this._id)
      }

    }
  });

}