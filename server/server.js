if (Meteor.isServer) {
  Meteor.startup(function() {
    // code to run on server at startup
    console.log("server is listening")
  });

  Meteor.publish("kids", function() {
    return Kids.find({
      owner: this.userId
    });
  })

  Meteor.publish("scores", function(){
    return Scores.find({kid_owner: this.userId}, {sort: {point_date:-1} });
  })

  Meteor.publish("rewards", function(){
    return Rewards.find({kid_owner:this.userId}, {sort: {required_points:-1}});
  })
}

Meteor.methods({
//***************************Kids Model ************************************
  add_kid: function(kidname, kidgrade, new_avatar, kid_born) {
    if (!Meteor.userId()) {
      throw new Meteor.error("not-authorized")
    }
    Kids.insert({
      kidname: kidname,
      kidgrade: kidgrade,
      createdAt: moment().format('L'),
      owner: Meteor.userId(),
      avatar: new_avatar,
    })

    return false;
  },



  delete_kid: function(kid) {
    if (!Meteor.userId()) {
      throw new Meteor.error("not-authorized")
    }
    Kids.remove(kid)
  },
//***************************Scores Model**********************************
  update_points: function(kid, point_date, point_value) {
    if (!Meteor.userId()) {
      throw new Meteor.error("not-authorized")
    }

    

    Scores.upsert({
      //selector from scores where kid = kid AND date = date
      kid:kid,
      point_date:point_date,
      kid_owner: Meteor.userId()
    },
      //modifier
    {
      $set: {point_value:point_value}
    })
  },

//**************************Rewards Model***********************************

  update_rewards: function(kid, reward_name, required_points){
    if(!Meteor.userId()){
      throw new Meteor.error("not-authorized")
    }

    Rewards.upsert({
      //selector
      kid:kid,
      reward_name:reward_name,
      kid_owner: Meteor.userId()
    },
    //modifier
    {
      $set:{required_points:required_points}
    })
    
  }

    
})
